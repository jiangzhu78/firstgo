package httpserver

import (
	"fmt"
	"log"
	"net/http"
)

var Conf Configuration


type Configuration struct {
    BuildNumber    string
    RevisionNumber   string
}

type Hello struct{
}

func (h Hello) ServeHTTP (
	w http.ResponseWriter,
	r *http.Request) {
	fmt.Fprint(w, "Congratulations! Here is the first GO! \n\tbuild number:"+Conf.BuildNumber+"\n\trevision number:"+Conf.RevisionNumber)
}

func Start(port int) {
	fmt.Println(port)
	var h Hello
	err := http.ListenAndServe(":" + fmt.Sprintf("%d", port), h)
	if err != nil {
		log.Fatal(err)
	}
}
