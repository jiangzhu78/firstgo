package main

import (
	"fmt"
	"flag"
	"firstgo/httpserver"
	"encoding/json"
    "os"
)


func main() {

	var port int

    flag.IntVar(&port, "port", 8080, "default port is 8080")
    flag.Parse()

	file, _ := os.Open("config.json")
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&httpserver.Conf)
	if err != nil {
	  fmt.Println("error:", err)
	} else {
		fmt.Println(httpserver.Conf)
	}
    httpserver.Start(port)

}