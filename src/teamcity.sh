#Build project
export GOPATH=%teamcity.build.checkoutDir%
go install firstgo/main

#Parse configuration file
cd $GOPATH/bin
cp ../src/config.json .
sed -i 's/{build.number}/%build.number%/g' config.json
sed -i 's/{revision.number}/%build.vcs.number%/g' config.json
